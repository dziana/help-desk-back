package help_desk_back.services;

import help_desk_back.models.entities.Privilege;
import help_desk_back.models.messages.RoleName;


import java.util.List;
import java.util.Optional;

public interface PrivilegeService{
    Privilege save(Privilege privilege);

    void delete(Privilege privilege);

    Optional<Privilege> findById(Long id);

    List<Privilege> findAll();

    Optional<Privilege> findByName(RoleName name);
}
