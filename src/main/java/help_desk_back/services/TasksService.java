package help_desk_back.services;

import help_desk_back.models.entities.Status;
import help_desk_back.models.entities.SystemUser;
import help_desk_back.models.entities.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

public interface TasksService {
    Task save(Task task);

    void delete(Task task);

    Optional<Task> findById(Long id);

    List<Task> findAll();

    Page<Task> findAllBySystemUser(SystemUser user, Pageable pageable);

    Page<Task> findAllByStatusAndOperator(Status status, Long operatorId, Pageable pageable);

    Page<Task> findAllByExecutor(SystemUser user, Pageable pageable);

    void deleteById(Long id);

    Task updateTaskStatus(Long taskId, Long statusId);

    Task updateTaskExecutor(Long taskId, Long executorId);

    Task assignTask(Long taskId, Long executorId, Timestamp dueDate, Long operatorId);

    Task executeTask(Long taskId);

    Task closeTask(Long taskId);

}
