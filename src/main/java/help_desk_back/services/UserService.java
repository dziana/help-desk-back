package help_desk_back.services;

import help_desk_back.models.entities.Privilege;
import help_desk_back.models.entities.Subdivision;
import help_desk_back.models.entities.SystemUser;
import org.springframework.http.HttpHeaders;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface UserService {
    SystemUser save(SystemUser systemUser);

    void delete(SystemUser users);

    Optional<SystemUser> findById(Long id);

    List<SystemUser> findExecutorsBySubdivision(Subdivision subdivision);

    Page<SystemUser> findAllPageable(Pageable pageable);

    List<SystemUser> findAllByPrivilege(Privilege role);

    List<SystemUser> findAll();

    void deleteById(Long id);

    Optional<SystemUser> findByEmail(String email);

    SystemUser updateUserPrivileges(Long id, List<Long> users);

    Boolean existsByEmail(String email);

    SystemUser getUserIdByToken(HttpHeaders token);

    SystemUser updateUser(Long id, SystemUser systemUser);
}
