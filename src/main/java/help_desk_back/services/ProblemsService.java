package help_desk_back.services;

import help_desk_back.models.entities.Problem;

import java.util.List;
import java.util.Optional;

public interface ProblemsService {
    Problem save(Problem problem);

    void delete(Problem problem);

    Optional<Problem> findById(Long id);

    List<Problem> findAll();

    void deleteById(Long id);
}
