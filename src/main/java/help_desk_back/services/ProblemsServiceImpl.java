package help_desk_back.services;

import help_desk_back.models.entities.Problem;
import help_desk_back.models.repositories.ProblemsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProblemsServiceImpl implements ProblemsService {
    private final ProblemsRepository problemsRepository;

    @Override
    public Optional<Problem> findById(Long id) {
        return problemsRepository.findById(id);
    }

    @Override
    public Problem save(Problem problem) {
        return problemsRepository.save(problem);
    }

    @Override
    public List<Problem> findAll() {
        return (List<Problem>) problemsRepository.findAll();
    }

    @Override
    public void delete(Problem saved) {
        problemsRepository.delete(saved);
    }

    @Override
    public void deleteById(Long id) {
        problemsRepository.deleteById(id);
    }
}
