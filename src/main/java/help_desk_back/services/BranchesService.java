package help_desk_back.services;


import help_desk_back.models.entities.Branch;

import java.util.List;
import java.util.Optional;

public interface BranchesService {
    Branch save(Branch branch);

    void delete(Branch branch);

    Optional<Branch> findById(Long id);

    List<Branch> findAll();

    void deleteById(Long id);

    Branch updateBranch(Long id, Branch branchIn);
}
