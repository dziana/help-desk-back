package help_desk_back.services;

import com.fasterxml.jackson.annotation.JsonIgnore;
import help_desk_back.models.entities.Privilege;
import help_desk_back.models.entities.SystemUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;
import java.util.stream.Collectors;

public class UserPrinciple implements UserDetails {
	private static final long serialVersionUID = 1L;

	private Long id;

    private String name;

    private String family;

    private String email;

    private Set<Privilege> privileges;

    @JsonIgnore
    private String password;

    private Collection<? extends GrantedAuthority> authorities;

    public UserPrinciple(Long id, String name,
			    		String family, Set<Privilege> privileges, String email, String password,
			    		Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.name = name;
        this.family = family;
        this.email = email;
        this.privileges = privileges;
        this.password = password;
        this.authorities = authorities;
    }

    public static UserPrinciple build(SystemUser user) {
        List<GrantedAuthority> authorities = user.getPrivilege().stream().map(role ->
                new SimpleGrantedAuthority(role.getName().name())
        ).collect(Collectors.toList());

        return new UserPrinciple(
                user.getId(),
                user.getName(),
                user.getFamily(),
                user.getPrivilege(),
                user.getEmail(),
                user.getPassword(),
                authorities
        );
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public List<String> getPrivileges () {
        List<String> list = new LinkedList<>();
        this.privileges.forEach(e -> {
            list.add(e.getName().name());
        });
        return list;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        UserPrinciple user = (UserPrinciple) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public String toString() {
        return "UserPrinciple{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", family='" + family + '\'' +
                ", email='" + email + '\'' +
                ", privileges=" + privileges +
                ", password='" + password + '\'' +
                ", authorities=" + authorities +
                '}';
    }
}