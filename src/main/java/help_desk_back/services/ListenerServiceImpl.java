package help_desk_back.services;

import help_desk_back.exceptions.ListenersNotFoundException;
import help_desk_back.models.entities.Listener;
import help_desk_back.models.repositories.ListenersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ListenerServiceImpl implements ListenerService {
    private final ListenersRepository listenersRepository;

    @Override
    public Optional<Listener> findById(Long id) {
        return listenersRepository.findById(id);
    }

    @Override
    public Listener save(Listener listener) {
        return listenersRepository.save(listener);
    }

    @Override
    public List<Listener> findAll() {
        return (List<Listener>) listenersRepository.findAll();
    }

    @Override
    public void delete(Listener listener) {
        listenersRepository.delete(listener);
    }

    @Override
    public void deleteById(Long id) {
        listenersRepository.deleteById(id);
    }

    @Override
    public Listener updateListener(Long id, Listener listenerIn) {
        Optional<Listener> listenerOptional = listenersRepository.findById(id);
        if (!listenerOptional.isPresent()) {
            throw new ListenersNotFoundException("--- Unable to find id = " +
                    id + " Record not found or Database not available ! ---");
        }
        Listener listener = listenerOptional.get();
        listener.setSubdivisionId(listenerIn.getSubdivisionId());
        listener.setSystemUserId(listenerIn.getSystemUserId());
        Listener savedListener = listenersRepository.save(listener);
        return savedListener;
    }
}
