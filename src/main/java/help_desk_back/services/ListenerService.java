package help_desk_back.services;


import help_desk_back.models.entities.Listener;

import java.util.List;
import java.util.Optional;

public interface ListenerService {
    Listener save(Listener listener);

    void delete(Listener listener);

    Optional<Listener> findById(Long id);

    List<Listener> findAll();

    void deleteById(Long id);

    Listener updateListener(Long id, Listener listener);
}
