package help_desk_back.services;

import com.sun.org.apache.xpath.internal.operations.Bool;
import help_desk_back.exceptions.UserNotFoundException;
import help_desk_back.jwt.JwtProvider;
import help_desk_back.models.entities.Privilege;
import help_desk_back.models.entities.Subdivision;
import help_desk_back.models.entities.SystemUser;
import help_desk_back.models.repositories.SystemUsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserService {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SystemUsersRepository usersRepository;

    @Autowired
    private JwtProvider tokenProvider;

    @Override
    public Optional<SystemUser> findById(Long id) {
        return usersRepository.findById(id);
    }

    @Override
    public SystemUser save(SystemUser systemUser) {
        return usersRepository.save(systemUser);
    }

    @Override
    public Page<SystemUser> findAllPageable(Pageable pageable) {
        return usersRepository.findAll(pageable);
    }

    @Override
    public List<SystemUser> findAllByPrivilege(Privilege privilege) {
        return (List<SystemUser>) usersRepository.findByPrivilege(privilege);
    }

    @Override
    public List<SystemUser> findAll() {
        return usersRepository.findAll();
    }

    @Override
    public List<SystemUser> findExecutorsBySubdivision(Subdivision subdivision) {
        Privilege privilege = new Privilege();
        privilege.setId(4L);
        return usersRepository.findByPrivilegeAndSubdivisionId(privilege, subdivision);
    }

    @Override
    public void delete(SystemUser saved) {
        usersRepository.delete(saved);
    }

    @Override
    public void deleteById(Long id) {
        usersRepository.deleteById(id);
    }

    @Override
    public Optional<SystemUser> findByEmail(String email) {
        return usersRepository.findByEmail(email);
    }

    @Override
    public Boolean existsByEmail(String email) {
        return usersRepository.existsByEmail(email);
    }

    @Override
    public SystemUser updateUserPrivileges(Long id, List<Long> userPriv) {
        Optional<SystemUser> usersOptional = usersRepository.findById(id);
        if (!usersOptional.isPresent()) {
            throw new UserNotFoundException("--- Unable to find id = " +
                    id + " Record not found or Database not available ! ---");
        } else {
            SystemUser systemUser;
            systemUser = usersOptional.get();

            Set<Privilege> set = new HashSet<>();
            for (Long privilegeId : userPriv) {
                Privilege pr = new Privilege();
                pr.setId(privilegeId);
                set.add(pr);
            }
            systemUser.setPrivilege(set);
            SystemUser userFromDb = usersRepository.save(systemUser);
            return userFromDb;
        }
    }

    @Override
    public SystemUser getUserIdByToken(HttpHeaders request) {
        String token = getJwt(request);
        String userEmail = tokenProvider.getUserNameFromJwtToken(token);
        Optional<SystemUser> systemUser = findByEmail(userEmail);
        if (!systemUser.isPresent()) {
            throw new UserNotFoundException("--- can't find user by token ---");
        }
        return systemUser.get();
    }

    @Override
    public SystemUser updateUser(Long id, SystemUser systemUserIn) {
        Optional<SystemUser> userOptional = usersRepository.findById(id);
        if (!userOptional.isPresent()) {
            throw new UserNotFoundException("--- Unable to find id = " +
                    id + " Record not found or Database not available ! ---");
        }
        SystemUser systemUser = userOptional.get();
        systemUser.setName(systemUserIn.getName());
        systemUser.setFamily(systemUserIn.getFamily());
        systemUser.setEmail(systemUserIn.getEmail());
        systemUser.setBranchId(systemUserIn.getBranchId());
        systemUser.setSubdivisionId(systemUserIn.getSubdivisionId());
        SystemUser savedUser = usersRepository.save(systemUser);
        return savedUser;
    }

    private String getJwt(HttpHeaders request) {
        String authHeader = request.get("Authorization").get(0);

        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.replace("Bearer ", "");
        }

        return null;
    }
}
