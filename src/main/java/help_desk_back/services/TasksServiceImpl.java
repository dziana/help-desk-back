package help_desk_back.services;

import help_desk_back.exceptions.TaskNotFoundException;
import help_desk_back.models.entities.Status;
import help_desk_back.models.entities.SystemUser;
import help_desk_back.models.entities.Task;
import help_desk_back.models.repositories.TasksRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TasksServiceImpl implements TasksService {
    private final TasksRepository tasksRepository;

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public Optional<Task> findById(Long id) {
        return tasksRepository.findById(id);
    }

    @Override
    public Task save(Task task) {
        Task savedTask = tasksRepository.save(task);
        entityManager.refresh(savedTask);
        return savedTask;
    }

    @Override
    public List<Task> findAll() {
        return (List<Task>) tasksRepository.findAll();
    }

    @Override
    public Page<Task> findAllBySystemUser(SystemUser user, Pageable pageable) {
        return tasksRepository.findAllBySystemUserId(user, pageable);
    }

    @Override
    public Page<Task> findAllByExecutor(SystemUser user, Pageable pageable) {
        Status status = new Status();
        status.setId(4L);
        return tasksRepository.findAllByExecutorIdAndStatusIdNot(user, status, pageable);
    }

    @Override
    public Page<Task> findAllByStatusAndOperator(Status status, Long operatorId, Pageable pageable) {
        return tasksRepository.findAllByListenerIdSystemUserIdIdAndStatusId(operatorId, status, pageable);
    }

    @Override
    public void delete(Task saved) {
        tasksRepository.delete(saved);
    }

    @Override
    public void deleteById(Long id) {
        tasksRepository.deleteById(id);
    }

    @Override
    public Task updateTaskStatus(Long taskId, Long statusId) {
        Optional<Task> taskOptional = tasksRepository.findById(taskId);
        if (!taskOptional.isPresent()) {
            throw new TaskNotFoundException("--- Unable to find id = " +
                    taskId + " Record not found or Database not available ! ---");
        }
        Task task = taskOptional.get();
        Status status = new Status();
        status.setId(statusId);
        task.setStatusId(status);

        Task savedTask = tasksRepository.save(task);
        return savedTask;
    }

    @Override
    public Task assignTask(Long taskId, Long executorId, Timestamp dueDate, Long operatorId) {
        Optional<Task> taskOptional = tasksRepository.findById(taskId);
        if (!taskOptional.isPresent()) {
            throw new TaskNotFoundException("--- Unable to find id = " +
                    taskId + " Record not found or Database not available ! ---");
        }
        Task task = taskOptional.get();

        Status status = new Status();
        status.setId(2L);
        task.setStatusId(status);

        if (executorId != null) {
            SystemUser systemUser = new SystemUser();
            systemUser.setId(executorId);
            task.setExecutorId(systemUser);
        } else {
            task.setExecutorId(null);
        }

        if (operatorId != null) {
            SystemUser operator = new SystemUser();
            operator.setId(operatorId);
            task.setOperatorId(operator);
        } else {
            task.setOperatorId(null);
        }

        task.setDueDate(dueDate);

        Task savedTask = tasksRepository.save(task);
        return savedTask;
    }

    @Override
    public Task updateTaskExecutor(Long taskId, Long executorId) {
        Optional<Task> taskOptional = tasksRepository.findById(taskId);
        if (!taskOptional.isPresent()) {
            throw new TaskNotFoundException("--- Unable to find id = " +
                    taskId + " Record not found or Database not available ! ---");
        }

        Task task = taskOptional.get();
        SystemUser systemUser = new SystemUser();
        systemUser.setId(executorId);
        task.setExecutorId(systemUser);

        Task savedTask = save(task);

        return savedTask;
    }

    @Override
    public Task executeTask(Long taskId) {
        Optional<Task> taskOptional = tasksRepository.findById(taskId);
        if (!taskOptional.isPresent()) {
            throw new TaskNotFoundException("--- Unable to find id = " +
                    taskId + " Record not found or Database not available ! ---");
        }
        Task task = taskOptional.get();
        Status status = new Status();
        status.setId(3L);
        task.setStatusId(status);
        task.setExecutedDate(new Date());
        Task savedTask = tasksRepository.save(task);
        return savedTask;
    }

    @Override
    public Task closeTask(Long taskId) {
        Optional<Task> taskOptional = tasksRepository.findById(taskId);
        if (!taskOptional.isPresent()) {
            throw new TaskNotFoundException("--- Unable to find id = " +
                    taskId + " Record not found or Database not available ! ---");
        }
        Task task = taskOptional.get();
        Status status = new Status();
        status.setId(4L);
        task.setStatusId(status);
        Task savedTask = tasksRepository.save(task);
        return savedTask;
    }
}
