package help_desk_back.services;

import help_desk_back.exceptions.ListenersNotFoundException;
import help_desk_back.models.entities.Subdivision;
import help_desk_back.models.repositories.SubdivisionsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SubdivisionsServiceImpl implements SubdivisionsService{
    private final SubdivisionsRepository subdivisionsRepository;

    @Override
    public Optional<Subdivision> findById(Long id) {
        return subdivisionsRepository.findById(id);
    }

    @Override
    public Subdivision save(Subdivision subdivision) {
        return subdivisionsRepository.save(subdivision);
    }

    @Override
    public List<Subdivision> findAll() {
        return (List<Subdivision>) subdivisionsRepository.findAll();
    }

    @Override
    public void delete(Subdivision saved) {
        subdivisionsRepository.delete(saved);
    }

    @Override
    public void deleteById(Long id) {
        subdivisionsRepository.deleteById(id);
    }

    @Override
    public Subdivision updateSubdivision(Long id, Subdivision branchIn) {
        Optional<Subdivision> branchOptional = subdivisionsRepository.findById(id);
        if (!branchOptional.isPresent()) {
            throw new ListenersNotFoundException("--- Unable to find id = " +
                    id + " Record not found or Database not available ! ---");
        }
        Subdivision subdivision = branchOptional.get();
        subdivision.setName(branchIn.getName());
        Subdivision savedBranch = subdivisionsRepository.save(subdivision);
        return savedBranch;
    }
}
