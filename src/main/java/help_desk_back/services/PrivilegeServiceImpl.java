package help_desk_back.services;

import help_desk_back.models.entities.Privilege;
import help_desk_back.models.messages.RoleName;
import help_desk_back.models.repositories.PrivilegesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PrivilegeServiceImpl implements PrivilegeService{
    private final PrivilegesRepository privilegesRepository;

    @Override
    public Optional<Privilege> findById(Long id) {
        return privilegesRepository.findById(id);
    }

    @Override
    public Optional<Privilege> findByName(RoleName name) {
        return privilegesRepository.findByName(name);
    }

    @Override
    public Privilege save(Privilege privilege) {
        return privilegesRepository.save(privilege);
    }

    @Override
    public List<Privilege> findAll() {
        return (List<Privilege>) privilegesRepository.findAll();
    }

    @Override
    public void delete(Privilege saved) {
        privilegesRepository.delete(saved);
    }
}
