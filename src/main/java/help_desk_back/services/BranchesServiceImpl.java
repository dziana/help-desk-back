package help_desk_back.services;

import help_desk_back.exceptions.ListenersNotFoundException;
import help_desk_back.models.entities.Branch;
import help_desk_back.models.entities.Listener;
import help_desk_back.models.repositories.BranchesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BranchesServiceImpl implements BranchesService {
    private final BranchesRepository branchesRepository;

    @Override
    public Optional<Branch> findById(Long id) {
        return branchesRepository.findById(id);
    }

    @Override
    public Branch save(Branch branch) {
        return branchesRepository.save(branch);
    }

    @Override
    public List<Branch> findAll() {
        return (List<Branch>) branchesRepository.findAll();
    }

    @Override
    public void delete(Branch saved) {
        branchesRepository.delete(saved);
    }

    @Override
    public void deleteById(Long id) {
        branchesRepository.deleteById(id);
    }

    @Override
    public Branch updateBranch(Long id, Branch branchIn) {
        Optional<Branch> branchOptional = branchesRepository.findById(id);
        if (!branchOptional.isPresent()) {
            throw new ListenersNotFoundException("--- Unable to find id = " +
                    id + " Record not found or Database not available ! ---");
        }
        Branch branch = branchOptional.get();
        branch.setName(branchIn.getName());
        branch.setAddress(branchIn.getAddress());
        Branch savedBranch = branchesRepository.save(branch);
        return savedBranch;
    }
}
