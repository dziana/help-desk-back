package help_desk_back.services;

import help_desk_back.models.entities.Subdivision;

import java.util.List;
import java.util.Optional;

public interface SubdivisionsService {
    Subdivision save(Subdivision subdivision);

    void delete(Subdivision subdivision);

    void deleteById(Long id);

    Optional<Subdivision> findById(Long id);

    List<Subdivision> findAll();

    Subdivision updateSubdivision(Long id, Subdivision subdivisionIn);
}
