package help_desk_back.controllers;

import help_desk_back.HelpDeskBackApplication;
import help_desk_back.exceptions.ListenersNotFoundException;
import help_desk_back.mappers.ListenerMapper;
import help_desk_back.models.entities.Listener;
import help_desk_back.models.messages.dtoIn.ListenerUpdateIn;
import help_desk_back.models.messages.report.ListenerReport;
import help_desk_back.services.ListenerService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@CrossOrigin(origins = "*", maxAge = 3600)
public class ListenersController {

    private static final Logger LOGGER = LogManager.getLogger(HelpDeskBackApplication.class);
    private final RestHelper restHelper;
    private final ListenerService listenersService;
    private final ListenerMapper listenerMapper;

    @GetMapping("/api/listeners")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public List<Listener> getListeners() {
        LOGGER.info("--- Listeners successfully got from DB. ---");
        return (listenersService.findAll());
    }

    @GetMapping("/api/listeners/report")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public List<ListenerReport> getListenersForReport() {
        LOGGER.info("--- Listeners successfully got from DB. ---");
        List<ListenerReport> listenerReportList = new ArrayList<>();
        listenersService.findAll().forEach(e -> {
            listenerReportList.add(listenerMapper.mapperListenerToListenerReport(e));
        });
        return listenerReportList;
    }

    @GetMapping("/api/listeners/{id}")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public Listener getListener(@PathVariable String id) {
        Optional<Listener> optionalListener = listenersService.findById(Long.valueOf(id));
        if (!optionalListener.isPresent()) {
            LOGGER.error("--- Can't get listener from DB. Listener by Id = " + id + " not found. ---");
            throw new ListenersNotFoundException("--- Unable to find id = " +
                    id + " Record not found or Database not available ! ---");
        } else {
            LOGGER.info("--- Listener by id = " + id + " successfully got from DB. ---");
            return optionalListener.get();
        }
    }

    @DeleteMapping("/api/listeners/{id}")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> deleteListener(@PathVariable String id) {
        Optional<Listener> optionalListener = listenersService.findById(Long.valueOf(id));
        if (!optionalListener.isPresent()) {
            LOGGER.error("--- Can't get listener from DB. Listener by Id = " + id + " not found. ---");
            throw new ListenersNotFoundException("--- Unable to delete listener! id = " + id + "' does not exist! ---");
        } else {
            listenersService.deleteById(Long.valueOf(id));
            LOGGER.info("--- Listener by id = " + id + " successfully deleted from DB. ---");
        }
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(id).toUri();
        return restHelper.createResponse(location, optionalListener.get(), "deleteListener");
    }

    @PostMapping(value = "/api/listeners", consumes = "application/json", produces = "application/json")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> saveListener(@RequestBody ListenerUpdateIn listenerIn) {
        Listener savedListener = listenersService.save(listenerMapper.mapperListenerUpdateIn(listenerIn));
        if (savedListener != null) {
            LOGGER.info("--- Listener created with Id  = '" + savedListener.getId() + "'. ---");
        }
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(savedListener.getId()).toUri();
        return restHelper.createResponse(location, savedListener, "saveListener");
    }

    @PatchMapping(value = "/api/listeners/{id}", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateStatus(@PathVariable String id, @RequestBody ListenerUpdateIn listenerIn) {
        Listener listener = listenersService.updateListener(Long.valueOf(id), listenerMapper.mapperListenerUpdateIn(listenerIn));
        LOGGER.info("--- Listener by id = " + id + " successfully updated. ---");
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(id).toUri();
        return restHelper.createResponse(location, listener, "updateListener");
    }
}
