package help_desk_back.controllers;

import help_desk_back.HelpDeskBackApplication;
import help_desk_back.exceptions.BranchesNotFoundException;
import help_desk_back.models.entities.Branch;
import help_desk_back.services.BranchesService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BranchesController {
    private final BranchesService branchesService;
    private static final Logger LOGGER = LogManager.getLogger(HelpDeskBackApplication.class);
    private final RestHelper restHelper;

    @GetMapping("/api/branches")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public List<Branch> getBranches() {
        return (branchesService.findAll());
    }

    @GetMapping("/api/branches/{id}")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public Branch getBranches (@PathVariable String id) {
        Optional<Branch> optionalBranch = branchesService.findById(Long.valueOf(id));
        if (!optionalBranch.isPresent()) {
            LOGGER.error("--- Can't get branch from DB. Branch by Id = " + id + " not found. ---");
            throw new BranchesNotFoundException("--- Unable to find id = " +
                    id + " Record not found or Database not available ! ---");
        } else {
            LOGGER.info("--- Branch by id = " + id + " successfully got from DB. ---");
            return optionalBranch.get();
        }
    }

    @DeleteMapping("/api/branches/{id}")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> deleteBranches (@PathVariable String id) {
        Optional<Branch> optionalBranch = branchesService.findById(Long.valueOf(id));
        if (!optionalBranch.isPresent()) {
            LOGGER.error("--- Can't get branch from DB. Branch by Id = " + id + " not found. ---");
            throw new BranchesNotFoundException("--- Unable to delete branch! id = " + id + "' does not exist! ---");
        } else {
            branchesService.deleteById(Long.valueOf(id));
            LOGGER.info("--- Branch by id = " + id + " successfully deleted from DB. ---");
        }
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(id).toUri();
        return restHelper.createResponse(location, optionalBranch.get(), "deleteBranch");
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value = "/api/branches", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> saveBranches (@RequestBody Branch branch) {
        Branch savedBranch = branchesService.save(branch);
        if (savedBranch != null) {
            LOGGER.info("--- Branch created with Id  = '" + savedBranch.getId() + "'. ---");
        }
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(savedBranch.getId()).toUri();
        return restHelper.createResponse(location, savedBranch, "saveBranch");
    }

    @PatchMapping(value = "/api/branches/{id}", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateStatus(@PathVariable String id, @RequestBody Branch branchIn) {
        Branch branch = branchesService.updateBranch(Long.valueOf(id), branchIn);
        LOGGER.info("--- Listener by id = " + id + " successfully updated. ---");
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(id).toUri();
        return restHelper.createResponse(location, branch, "updateBranch");
    }
}
