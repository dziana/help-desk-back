package help_desk_back.controllers;

import help_desk_back.HelpDeskBackApplication;
import help_desk_back.exceptions.UserNotFoundException;
import help_desk_back.mappers.UserMapper;
import help_desk_back.models.entities.Privilege;
import help_desk_back.models.entities.Subdivision;
import help_desk_back.models.entities.SystemUser;
import help_desk_back.models.entities.Task;
import help_desk_back.models.messages.dtoIn.SaveUser;
import help_desk_back.models.messages.dtoIn.TaskUpdateValueIn;
import help_desk_back.models.messages.dtoIn.UserPrivilegeDtoIn;
import help_desk_back.models.messages.dtoOut.UserPageDtoOut;
import help_desk_back.models.messages.report.UserReport;
import help_desk_back.services.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {
    private static final Logger LOGGER = LogManager.getLogger(HelpDeskBackApplication.class);
    private final RestHelper restHelper;
    private final UserMapper userMapper;
    @Autowired
    private final UserService userService;

    @GetMapping("/api/users")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public UserPageDtoOut getUsers(Pageable pageable) {
        LOGGER.info("--- Users successfully got from DB. ---");
        return userMapper.mapperPageToUserPageDtoOut(userService.findAllPageable(pageable));
    }

    @GetMapping("/api/users/report")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public List<UserReport> getUsers() {
        LOGGER.info("--- Users successfully got from DB. ---");
        List<UserReport> userReportList = new ArrayList<>();
        userService.findAll().forEach(e -> {
            userReportList.add(userMapper.mapperUserToUserReport(e));
        });
        return userReportList;
    }

    @GetMapping("/api/users/privilege")
    @ResponseBody
    @PreAuthorize("hasRole('USER')")
    public List<SystemUser> getUsersByPrivilege(@RequestParam("privilege") String privilege) {
        Privilege priv = new Privilege();
        priv.setId(Long.valueOf(privilege));
        LOGGER.info("--- Users successfully got from DB by privilege" + privilege + ". ---");
        return userService.findAllByPrivilege(priv);
    }

    @GetMapping("/api/users/subdivision")
    @ResponseBody
    @PreAuthorize("hasRole('USER')")
    public List<SystemUser> getExecutorsBySubdivision(
            @RequestHeader HttpHeaders headers) {
        SystemUser user = userService.getUserIdByToken(headers);
        LOGGER.info("--- Users successfully got from DB by subdivision" + user.getSubdivisionId().getName() + ". ---");
        return userService.findExecutorsBySubdivision(user.getSubdivisionId());
    }

    @GetMapping("/api/users/{id}")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public SystemUser getUser(@PathVariable String id) {
        Optional<SystemUser> optionalUser = userService.findById(Long.valueOf(id));
        if (!optionalUser.isPresent()) {
            LOGGER.error("--- Can't get user from DB. User by Id = " + id + " not found. ---");
            throw new UserNotFoundException("--- Unable to find id = " +
                    id + " Record not found or Database not available ! ---");
        } else {
            LOGGER.info("--- User by id = " + id + " successfully got from DB. ---");
            return optionalUser.get();
        }
    }

    @GetMapping("/api/users/email")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public SystemUser getUserByEmail(String email) {
        Optional<SystemUser> optionalUser = userService.findByEmail(email);
        if (!optionalUser.isPresent()) {
            LOGGER.error("--- Can't get user from DB. User by Email = " + email + " not found. ---");
            throw new UserNotFoundException("--- Unable to find email = " +
                    email + " Record not found or Database not available ! ---");
        } else {
            LOGGER.info("--- User by Email = " + email + " successfully got from DB. ---");
            return optionalUser.get();
        }
    }

    @DeleteMapping("/api/users/{id}")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> deleteUser(@PathVariable String id) {
        Optional<SystemUser> usersOptional = userService.findById(Long.valueOf(id));
        if (!usersOptional.isPresent()) {
            LOGGER.error("--- Can't get user from DB. User by Id = " + id + " not found. ---");
            throw new UserNotFoundException("--- Unable to delete user! id = " + id + "' does not exist! ---");
        } else {
            userService.deleteById(Long.valueOf(id));
            LOGGER.info("--- User by id = " + id + " successfully deleted from DB. ---");
        }

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(id).toUri();
        return restHelper.createResponse(location, null, "deleteUser");
    }

    @PatchMapping(value = "/api/users/{id}", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateStatus(@PathVariable String id, @RequestBody SaveUser userIn) {
        SystemUser user = userService.updateUser(Long.valueOf(id), userMapper.mapperSignupUserToSystemUser(userIn));
        LOGGER.info("--- User by id = " + id + " successfully updated. ---");
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(user.getId()).toUri();
        return restHelper.createResponse(location, user, "updateUser");
    }

    @PatchMapping(value = "/api/users/{id}/privileges", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateStatus(@PathVariable String id, @RequestBody UserPrivilegeDtoIn privileges) {
        SystemUser user = userService.updateUserPrivileges(Long.valueOf(id), privileges.getPrivilegeList());
        LOGGER.info("--- User by id = " + id + " successfully updated. ---");
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(user.getId()).toUri();
        return restHelper.createResponse(location, user, "updateUser");
    }
}
