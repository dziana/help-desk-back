package help_desk_back.controllers;

import help_desk_back.HelpDeskBackApplication;
import help_desk_back.exceptions.TaskNotFoundException;
import help_desk_back.mappers.TaskMapper;
import help_desk_back.models.entities.Status;
import help_desk_back.models.entities.SystemUser;
import help_desk_back.models.entities.Task;
import help_desk_back.models.messages.dtoIn.AssignTaskValueIn;
import help_desk_back.models.messages.dtoIn.TaskDtoIn;
import help_desk_back.models.messages.dtoIn.TaskUpdateValueIn;
import help_desk_back.models.messages.dtoOut.TaskDtoOut;
import help_desk_back.models.messages.dtoOut.TaskPageDtoOut;
import help_desk_back.models.messages.report.TaskReport;
import help_desk_back.services.TasksService;
import help_desk_back.services.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TasksController {
    private static final Logger LOGGER = LogManager.getLogger(HelpDeskBackApplication.class);
    private final RestHelper restHelper;
    private final TasksService tasksService;
    private final UserService userService;
    private final TaskMapper taskMapper;

    @GetMapping("/api/tasks/personal")
    @ResponseBody
    @PreAuthorize("hasRole('USER')")
    TaskPageDtoOut list(Pageable pageable, @RequestHeader HttpHeaders headers) {
        SystemUser user = userService.getUserIdByToken(headers);
        Page<Task> tasks = tasksService.findAllBySystemUser(user, pageable);
        return taskMapper.mapperPageToTaskPageDtoOut(tasks);
    }

    @GetMapping("/api/tasks/report")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    List<TaskReport> list() {
        List<Task> tasks = tasksService.findAll();
        List<TaskReport> taskReportList = new ArrayList<>();
        tasksService.findAll().forEach(e -> {
            taskReportList.add(taskMapper.mapperTasksToTaskReport(e));
        });
        return taskReportList;
    }

    @GetMapping("/api/tasks/perform")
    @ResponseBody
    @PreAuthorize("hasRole('EXECUTOR')")
    public TaskPageDtoOut getTasksToPerform(Pageable pageable, @RequestHeader HttpHeaders headers) {
        SystemUser user = userService.getUserIdByToken(headers);
        Page<Task> tasks = tasksService.findAllByExecutor(user, pageable);
        LOGGER.info("--- Tasks to perform successfully got from DB. ---");
        return taskMapper.mapperPageToTaskPageDtoOut(tasks);
    }

    @GetMapping("/api/tasks/status")
    @ResponseBody
    @PreAuthorize("hasRole('OPERATOR')")
    public TaskPageDtoOut getTasksByStatusAndOperatorId(
            Pageable pageable,
            @RequestParam("status") String statusId,
            @RequestHeader HttpHeaders headers
    ) {
        SystemUser user = userService.getUserIdByToken(headers);
        Status status = new Status();
        status.setId(Long.valueOf(statusId));
        Page<Task> tasks = tasksService.findAllByStatusAndOperator(status, user.getId(), pageable);
        LOGGER.info("--- Tasks by status" + statusId + "successfully got from DB. ---");
        return taskMapper.mapperPageToTaskPageDtoOut(tasks);
    }

    @DeleteMapping("/api/tasks/{id}")
    @ResponseBody
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<Object> deleteTask(@PathVariable String id) {
        Optional<Task> tasksOptional = tasksService.findById(Long.valueOf(id));
        if (!tasksOptional.isPresent()) {
            LOGGER.error("--- Can't get task from DB. Task by Id = " + id + " not found. ---");
            throw new TaskNotFoundException("--- Unable to delete task! id = " + id + "' does not exist! ---");
        } else {
            LOGGER.info("--- Task by id = " + id + " successfully got from DB. ---");
            tasksService.deleteById(Long.valueOf(id));
        }
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(id).toUri();
        return restHelper.createResponse(location, tasksOptional.get(), "deleteTask");
    }

    @PatchMapping(value = "/api/tasks/{id}/executor", consumes = "application/json", produces = "application/json")
    @PreAuthorize("hasRole('OPERATOR')")
    public ResponseEntity<Object> updateExecutor(@PathVariable String id, @RequestBody TaskUpdateValueIn executorIn) {
        Task task = tasksService.updateTaskExecutor(Long.valueOf(id), executorIn.getId());
        LOGGER.info("--- Task by id = " + id + " successfully updated. ---");
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(task.getId()).toUri();
        return restHelper.createResponse(location, task, "updateTask");
    }

    @PatchMapping(value = "/api/tasks/{id}/assign", consumes = "application/json", produces = "application/json")
    @PreAuthorize("hasRole('OPERATOR')")
    public ResponseEntity<Object> assignTask(@PathVariable String id, @RequestBody AssignTaskValueIn assignTaskIn, @RequestHeader HttpHeaders headers) {
        SystemUser user = userService.getUserIdByToken(headers);
        Task task = tasksService.assignTask(Long.valueOf(id), assignTaskIn.getExecutorId(), assignTaskIn.getDueDate(), user.getId());
        LOGGER.info("--- Task by id = " + id + " successfully updated. ---");
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(task.getId()).toUri();
        return restHelper.createResponse(location, task, "assignTask");
    }

    @PatchMapping(value = "/api/tasks/{id}/execute", consumes = "application/json", produces = "application/json")
    @PreAuthorize("hasRole('OPERATOR')")
    public ResponseEntity<Object> executeTask(@PathVariable String id) {
        Task task = tasksService.executeTask(Long.valueOf(id));
        LOGGER.info("--- Task by id = " + id + " successfully updated. ---");
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(task.getId()).toUri();
        return restHelper.createResponse(location, task, "updateTask");
    }

    @PatchMapping(value = "/api/tasks/{id}/close", consumes = "application/json", produces = "application/json")
    @PreAuthorize("hasRole('OPERATOR')")
    public ResponseEntity<Object> closeTask(@PathVariable String id) {
        Task task = tasksService.closeTask(Long.valueOf(id));
        LOGGER.info("--- Task by id = " + id + " successfully updated. ---");
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(task.getId()).toUri();
        return restHelper.createResponse(location, task, "updateTask");
    }

    @PatchMapping(value = "/api/tasks/{id}/status", consumes = "application/json", produces = "application/json")
    @PreAuthorize("hasRole('OPERATOR')")
    public ResponseEntity<Object> updateStatus(@PathVariable String id, @RequestBody TaskUpdateValueIn statusIn) {
        Task task = tasksService.updateTaskStatus(Long.valueOf(id), statusIn.getId());
        LOGGER.info("--- Task by id = " + id + " successfully updated. ---");
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(task.getId()).toUri();
        return restHelper.createResponse(location, task, "updateTask");
    }

    @PostMapping(value = "/api/tasks", consumes = "application/json", produces = "application/json")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<Object> saveTask(@RequestBody TaskDtoIn task) {
        Task savedTask = tasksService.save(taskMapper.mapperTaskDtoInToTask(task));
        if (savedTask != null) {
            LOGGER.info("--- Task created with Id  = '" + savedTask.getId() + "'. ---");
        }
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(savedTask.getId()).toUri();
        return restHelper.createResponse(location, taskMapper.mapperTasksToTaskDtoOut(savedTask), "saveTask");
    }

//    @GetMapping("/api/tasks/{id}")
//    @ResponseBody
//    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
//    public Task getTask(@PathVariable String id) {
//        Optional<Task> taskOptional = tasksService.findById(Long.valueOf(id));
//        if (!taskOptional.isPresent()) {
//            LOGGER.error("--- Can't get task from DB. Task by Id = " + id + " not found. ---");
//            throw new TaskNotFoundException("--- Unable to find id = " +
//                    id + " Record not found or Database not available ! ---");
//        } else {
//            LOGGER.info("--- Subdivision by id = " + id + " successfully got from DB. ---");
//            return taskOptional.get();
//        }
//    }
}
