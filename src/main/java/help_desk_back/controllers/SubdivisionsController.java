package help_desk_back.controllers;

import help_desk_back.HelpDeskBackApplication;
import help_desk_back.exceptions.SubdivisionNotFoundException;
import help_desk_back.models.entities.Subdivision;
import help_desk_back.services.SubdivisionsService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SubdivisionsController {
    private final SubdivisionsService subdivisionsService;
    private static final Logger LOGGER = LogManager.getLogger(HelpDeskBackApplication.class);
    private final RestHelper restHelper;

    @GetMapping("/api/subdivisions")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN') or hasRole('EXECUTOR')")
    public List<Subdivision> getSubdivisions() {
        LOGGER.info("--- Subdivisions successfully got from DB. ---");
        return (subdivisionsService.findAll());
    }

    @GetMapping("/api/subdivisions/{id}")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public Subdivision getSubdivision(@PathVariable String id) {
        Optional<Subdivision> optionalSubdivision = subdivisionsService.findById(Long.valueOf(id));
        if (!optionalSubdivision.isPresent()) {
            LOGGER.error("--- Can't get subdivision from DB. Subdivision by Id = " + id + " not found. ---");
            throw new SubdivisionNotFoundException("--- Unable to find id = " +
                    id + " Record not found or Database not available ! ---");
        } else {
            LOGGER.info("--- Subdivision by id = " + id + " successfully got from DB. ---");
            return optionalSubdivision.get();
        }
    }

    @DeleteMapping("/api/subdivisions/{id}")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> deleteSubdivision(@PathVariable String id) {
        Optional<Subdivision> optionalSubdivision = subdivisionsService.findById(Long.valueOf(id));
        if (!optionalSubdivision.isPresent()) {
            LOGGER.error("--- Can't get subdivision from DB. Subdivision by Id = " + id + " not found. ---");
            throw new SubdivisionNotFoundException("--- Unable to delete subdivision! id = " + id + "' does not exist! ---");
        } else {
            subdivisionsService.deleteById(Long.valueOf(id));
            LOGGER.info("--- Subdivision by id = " + id + " successfully deleted from DB. ---");
        }
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(id).toUri();
        return restHelper.createResponse(location, optionalSubdivision.get(), "deleteSubdivision");
    }

    @PostMapping(value = "/api/subdivisions", consumes = "application/json", produces = "application/json")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> saveSubdivision(@RequestBody Subdivision subdivision) {
        Subdivision savedSubdivision = subdivisionsService.save(subdivision);
        if (savedSubdivision != null) {
            LOGGER.info("--- Subdivision created with Id  = '" + savedSubdivision.getId() + "'. ---");
        }
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(savedSubdivision.getId()).toUri();
        return restHelper.createResponse(location, savedSubdivision, "saveSubdivision");
    }

    @PatchMapping(value = "/api/subdivisions/{id}", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateStatus(@PathVariable String id, @RequestBody Subdivision subdivisionIn) {
        Subdivision subdivision = subdivisionsService.updateSubdivision(Long.valueOf(id), subdivisionIn);
        LOGGER.info("--- Listener by id = " + id + " successfully updated. ---");
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(id).toUri();
        return restHelper.createResponse(location, subdivision, "updateSubdivision");
    }
}
