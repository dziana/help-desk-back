package help_desk_back.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.net.URI;

@Component
public class RestHelper {

    public ResponseEntity<Object> createResponse(URI location, Object returnObject, String methodName) {
        HttpHeaders resp = new HttpHeaders();
        resp.setLocation(location);
        return ResponseEntity
                .created(location)
                .header("ResponseHeader", "Controller Method : " + methodName + "();")
                .body(returnObject);
    }
}
