package help_desk_back.controllers;

import help_desk_back.HelpDeskBackApplication;
import help_desk_back.exceptions.ProblemNotFoundException;
import help_desk_back.jwt.JwtProvider;
import help_desk_back.models.entities.Problem;
import help_desk_back.models.entities.SystemUser;
import help_desk_back.services.ProblemsService;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProblemsController {

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    private static final Logger LOGGER = LogManager.getLogger(HelpDeskBackApplication.class);
    private final RestHelper restHelper;
    private final ProblemsService problemsService;

    @GetMapping("/api/problems")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public List<Problem> getProblems(@RequestHeader HttpHeaders headers) {
        LOGGER.info("--- Problems successfully got from DB. ---");
        return (problemsService.findAll());
    }

    @DeleteMapping("/api/problems/{id}")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> deleteProblem(@PathVariable String id) {
        Optional<Problem> optionalProblem = problemsService.findById(Long.valueOf(id));
        if (!optionalProblem.isPresent()) {
            LOGGER.error("--- Can't get problem from DB. Problem by Id = " + id + " not found. ---");
            throw new ProblemNotFoundException("--- Unable to delete problem! id = " + id + "' does not exist! ---");
        } else {
            problemsService.deleteById(Long.valueOf(id));
            LOGGER.info("--- Problem by id = " + id + " successfully deleted from DB. ---");
        }
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(id).toUri();
        return restHelper.createResponse(location, optionalProblem.get(), "deleteProblem");
    }

    @PostMapping(value = "/api/problems", consumes = "application/json", produces = "application/json")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> saveProblem(@RequestBody Problem problem) {
        Problem savedProblem = problemsService.save(problem);
        if (savedProblem != null) {
            LOGGER.info("--- Problem created with Id  = '" + savedProblem.getId() + "'. ---");
        }
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(savedProblem.getId()).toUri();
        return restHelper.createResponse(location, problem, "saveProblem");
    }
}
