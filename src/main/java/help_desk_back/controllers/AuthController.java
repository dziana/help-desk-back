package help_desk_back.controllers;

import help_desk_back.HelpDeskBackApplication;
import help_desk_back.exceptions.EmailIsUsedException;
import help_desk_back.jwt.JwtProvider;
import help_desk_back.mappers.UserMapper;
import help_desk_back.models.entities.SystemUser;
import help_desk_back.models.messages.dtoIn.LoginUser;
import help_desk_back.models.messages.dtoIn.SaveUser;
import help_desk_back.models.messages.dtoOut.JwtResponse;
import help_desk_back.services.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private static final Logger LOGGER = LogManager.getLogger(HelpDeskBackApplication.class);
    private final UserService userService;
    private final JwtProvider jwtProvider;
    private final UserMapper userMapper;
    private final RestHelper restHelper;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginUser loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateJwtToken(authentication);
        LOGGER.info("--- Loged in successfully for user " + loginRequest.getEmail() +" . ---");
        return ResponseEntity.ok(new JwtResponse(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<Object> registerUser(@Valid @RequestBody SaveUser signUpRequest) {
        if (userService.existsByEmail(signUpRequest.getEmail())) {
            throw new EmailIsUsedException("--- Email is already in use ---");
        }
        SystemUser user = userMapper.mapperSignupUserToSystemUser(signUpRequest);
        SystemUser savedUser = userService.save(user);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(user.getId()).toUri();
        LOGGER.info("--- Signed up successfully for user " + signUpRequest.getEmail() +" . ---");
        return restHelper.createResponse(location, savedUser, "registerUser");
    }
}
