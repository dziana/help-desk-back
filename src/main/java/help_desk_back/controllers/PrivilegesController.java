package help_desk_back.controllers;

import help_desk_back.HelpDeskBackApplication;
import help_desk_back.models.entities.Privilege;
import help_desk_back.services.PrivilegeService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PrivilegesController {
    private static final Logger LOGGER = LogManager.getLogger(HelpDeskBackApplication.class);
    private final PrivilegeService privilegeService;

    @GetMapping("/api/privileges")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public List<Privilege> getPrivileges() {
        LOGGER.info("--- Privileges successfully got from DB. ---");
        return (privilegeService.findAll());
    }
}
