package help_desk_back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:/application.properties")
@SpringBootApplication
public class HelpDeskBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelpDeskBackApplication.class, args);
    }

}

