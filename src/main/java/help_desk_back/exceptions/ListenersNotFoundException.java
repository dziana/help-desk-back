package help_desk_back.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ListenersNotFoundException extends RuntimeException {
    public ListenersNotFoundException(String exception) {
        super(exception);
    }
}
