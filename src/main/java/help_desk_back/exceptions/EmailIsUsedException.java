package help_desk_back.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EmailIsUsedException extends RuntimeException{
    public EmailIsUsedException(String exception) {
        super(exception);
    }
}
