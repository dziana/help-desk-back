package help_desk_back.mappers;

import help_desk_back.models.entities.*;
import help_desk_back.models.messages.dtoOut.ShortUserDto;
import help_desk_back.models.messages.dtoIn.TaskDtoIn;
import help_desk_back.models.messages.dtoOut.TaskDtoOut;
import help_desk_back.models.messages.dtoOut.TaskPageDtoOut;
import help_desk_back.models.messages.report.TaskReport;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class TaskMapper {
    public Task mapperTaskDtoInToTask(TaskDtoIn taskIn) {

        Task task = new Task();
        task.setName(taskIn.getName());
        task.setText(taskIn.getText());

        if (taskIn.getOperatorId() != null) {
            SystemUser operator = new SystemUser();
            operator.setId(taskIn.getOperatorId());
            task.setOperatorId(operator);
        } else {
            task.setOperatorId(null);
        }

        if (taskIn.getExecutorId() != null) {
            SystemUser executor = new SystemUser();
            executor.setId(taskIn.getExecutorId());
            task.setExecutorId(executor);
        } else {
            task.setExecutorId(null);
        }

        if (taskIn.getSystemUserId() != null) {
            SystemUser systemUser = new SystemUser();
            systemUser.setId(taskIn.getSystemUserId());
            task.setSystemUserId(systemUser);
        } else {
            task.setSystemUserId(null);
        }

        if (taskIn.getListenerId() != null) {
            Listener listener = new Listener();
            listener.setId(taskIn.getListenerId());
            task.setListenerId(listener);
        } else {
            task.setListenerId(null);
        }

        if (taskIn.getStatusId() != null) {
            Status status = new Status();
            status.setId(taskIn.getStatusId());
            task.setStatusId(status);
        } else {
            task.setStatusId(null);
        }

        task.setCreatedDate(new Date());
        return task;
    }

    public TaskDtoOut mapperTasksToTaskDtoOut(Task task) {
        TaskDtoOut taskDtoOut = new TaskDtoOut();
        taskDtoOut.setId(task.getId());
        taskDtoOut.setName(task.getName());
        taskDtoOut.setText(task.getText());
        taskDtoOut.setListenerId(task.getListenerId());
        taskDtoOut.setStatusId(task.getStatusId());
        ShortUserDto operator = fillShortUser(task.getOperatorId());
        taskDtoOut.setOperatorId(operator);
        taskDtoOut.setExecutorId(fillShortUser(task.getExecutorId()));
        taskDtoOut.setSystemUserId(fillShortUser(task.getSystemUserId()));
        taskDtoOut.setCreatedDate(task.getCreatedDate());
        taskDtoOut.setExecutedDate(task.getExecutedDate());
        taskDtoOut.setDueDate(task.getDueDate());
        return taskDtoOut;
    }

    public TaskReport mapperTasksToTaskReport(Task task) {
        TaskReport taskReport = new TaskReport();
        taskReport.setId(task.getId());
        taskReport.setName(task.getName());
        taskReport.setText(task.getText());
        taskReport.setCreatedDate(task.getCreatedDate());
        taskReport.setExecutedDate(task.getExecutedDate());
        taskReport.setDueDate(task.getDueDate());
        if (task.getListenerId() != null) {
            taskReport.setListenerId(task.getListenerId().getId());
        } else {
            taskReport.setListenerId(null);
        }
        taskReport.setStatus(task.getStatusId().getName());
        if (task.getOperatorId() != null) {
            taskReport.setOperatorId(fillShortUser(task.getOperatorId()).getId());
        } else {
            taskReport.setOperatorId(null);
        }
        if (task.getExecutorId() != null) {
            taskReport.setExecutorId(fillShortUser(task.getExecutorId()).getId());
        } else {
            taskReport.setExecutorId(null);
        }
        if (task.getSystemUserId() != null) {
            taskReport.setSystemUserId(fillShortUser(task.getSystemUserId()).getId());
        } else {
            taskReport.setSystemUserId(null);
        }
        return taskReport;
    }

    public TaskPageDtoOut mapperPageToTaskPageDtoOut(Page<Task> page) {
        TaskPageDtoOut taskPageDtoOut = new TaskPageDtoOut();
        List<TaskDtoOut> listTask = new ArrayList<>();
        page.getContent().forEach(e -> {
            listTask.add(mapperTasksToTaskDtoOut(e));
        });
        taskPageDtoOut.setContent(listTask);
        taskPageDtoOut.setTotalPages(page.getTotalElements());
        return taskPageDtoOut;
    }

    private ShortUserDto fillShortUser(SystemUser userIn) {
        if (userIn == null) {
            return null;
        }
        ShortUserDto systemUser = new ShortUserDto();
        systemUser.setId(userIn.getId());
        systemUser.setName(userIn.getName());
        systemUser.setFamily(userIn.getFamily());
        systemUser.setEMail(userIn.getEmail());
        return systemUser;
    }
}
