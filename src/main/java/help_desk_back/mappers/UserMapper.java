package help_desk_back.mappers;

import help_desk_back.models.entities.Branch;
import help_desk_back.models.entities.Privilege;
import help_desk_back.models.entities.Subdivision;
import help_desk_back.models.entities.SystemUser;
import help_desk_back.models.messages.RoleName;
import help_desk_back.models.messages.dtoIn.SaveUser;
import help_desk_back.models.messages.dtoOut.UserPageDtoOut;
import help_desk_back.models.messages.report.UserReport;
import help_desk_back.models.repositories.PrivilegesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserMapper {
    @Autowired
    PasswordEncoder encoder;

    @Autowired
    PrivilegesRepository privilegesRepository;

    public SystemUser mapperSignupUserToSystemUser(SaveUser userIn) {
        SystemUser systemUser = new SystemUser();
        systemUser.setName(userIn.getName().trim());
        systemUser.setFamily(userIn.getFamily().trim());
        systemUser.setEmail(userIn.getEmail().trim());
        systemUser.setPassword(encoder.encode(userIn.getPassword()));
        systemUser.setBranchId(createBranchById(userIn.getBranchId()));
        systemUser.setSubdivisionId(createSubdivisionById(userIn.getSubdivisionId()));
        Set<Privilege> setRoles = new HashSet<>();
        Privilege userRole = privilegesRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
        setRoles.add(userRole);
        systemUser.setPrivilege(setRoles);
        return systemUser;
    }

    public UserReport mapperUserToUserReport(SystemUser user) {
        UserReport userReport = new UserReport();
        userReport.setId(user.getId());
        userReport.setName(user.getName());
        userReport.setFamily(user.getFamily());
        userReport.setEMail(user.getEmail());
        StringBuilder privilege = new StringBuilder();
        for (Privilege priv : user.getPrivilege()) {
            privilege.append(priv.getName().name());
            privilege.append(" ");
        }
        userReport.setPrivilegeName(privilege.toString());
        if (user.getSubdivisionId() != null) {
            userReport.setSubdivisionId(user.getSubdivisionId().getId());
        } else {
            userReport.setSubdivisionId(null);
        }
        if (user.getBranchId() != null) {
            userReport.setBranchid(user.getBranchId().getId());
        } else {
            userReport.setBranchid(null);
        }
        return userReport;
    }

    private Branch createBranchById(Long id) {
        if (id == null) {
            return null;
        }
        Branch branch = new Branch();
        branch.setId(id);
        return branch;
    }

    private Subdivision createSubdivisionById(Long id) {
        if (id == null) {
            return null;
        }
        Subdivision subdivision = new Subdivision();
        subdivision.setId(id);
        return subdivision;
    }

    public UserPageDtoOut mapperPageToUserPageDtoOut(Page<SystemUser> page) {
        UserPageDtoOut taskPageDtoOut = new UserPageDtoOut();
        taskPageDtoOut.setContent(page.getContent());
        taskPageDtoOut.setTotalPages(page.getTotalElements());
        return taskPageDtoOut;
    }
}
