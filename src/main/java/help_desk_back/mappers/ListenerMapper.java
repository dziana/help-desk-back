package help_desk_back.mappers;

import help_desk_back.models.entities.Listener;
import help_desk_back.models.entities.Status;
import help_desk_back.models.entities.Subdivision;
import help_desk_back.models.entities.SystemUser;
import help_desk_back.models.messages.dtoIn.ListenerUpdateIn;
import help_desk_back.models.messages.dtoIn.TaskDtoIn;
import help_desk_back.models.messages.report.ListenerReport;
import org.springframework.stereotype.Component;

@Component
public class ListenerMapper {
    public Listener mapperListenerUpdateIn(ListenerUpdateIn listenerIn) {
        Listener listener = new Listener();
        Subdivision subdivision = new Subdivision();
        subdivision.setId(listenerIn.getSubdivisionId());
        listener.setSubdivisionId(subdivision);

        SystemUser systemUser = new SystemUser();
        systemUser.setId(listenerIn.getSystemUserId());
        listener.setSystemUserId(systemUser);

        return listener;
    }

    public ListenerReport mapperListenerToListenerReport(Listener listener) {
        ListenerReport listenerReport = new ListenerReport();
        listenerReport.setId(listener.getId());
        listenerReport.setSubdivisionId(listener.getSubdivisionId().getId());
        listenerReport.setSystemUserId(listener.getSystemUserId().getId());

        return listenerReport;
    }
}
