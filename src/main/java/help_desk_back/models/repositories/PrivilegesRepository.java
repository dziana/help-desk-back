package help_desk_back.models.repositories;

import help_desk_back.models.entities.Privilege;
import help_desk_back.models.messages.RoleName;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
@Repository
public interface PrivilegesRepository extends CrudRepository<Privilege, Long> {
    Optional<Privilege> findByName(RoleName roleName);
}
