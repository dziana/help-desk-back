package help_desk_back.models.repositories;

import help_desk_back.models.entities.Problem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface ProblemsRepository extends CrudRepository<Problem, Long> {
}
