package help_desk_back.models.repositories;

import help_desk_back.models.entities.Privilege;
import help_desk_back.models.entities.Subdivision;
import help_desk_back.models.entities.SystemUser;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Transactional
@Repository
public interface SystemUsersRepository extends PagingAndSortingRepository<SystemUser, Long> {
    Optional<SystemUser> findByEmail(String email);
    Boolean existsByEmail(String email);
    List<SystemUser> findByPrivilege(Privilege privilege);
    Page<SystemUser> findAll(Pageable pageable);
    List<SystemUser> findAll();
    List<SystemUser> findByPrivilegeAndSubdivisionId(Privilege privilege, Subdivision subdivision);
}
