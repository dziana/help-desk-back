package help_desk_back.models.repositories;

import help_desk_back.models.entities.Status;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface StatusRepository extends CrudRepository<Status, Long> {
}
