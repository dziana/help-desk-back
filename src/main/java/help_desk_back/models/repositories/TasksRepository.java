package help_desk_back.models.repositories;

import help_desk_back.models.entities.Status;
import help_desk_back.models.entities.SystemUser;
import help_desk_back.models.entities.Task;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Transactional
@Repository
public interface TasksRepository extends PagingAndSortingRepository<Task, Long> {
    List<Task> findAllByExecutorId(SystemUser systemUser);

    Page<Task> findAllByExecutorIdAndStatusIdNot(SystemUser systemUser, Status status, Pageable pageable);

    Page<Task> findAllByListenerIdSystemUserIdIdAndStatusId(Long systemUser, Status status, Pageable pageable);

    List<Task> findAllByStatusId(Status status);

    Page<Task> findAllBySystemUserId(SystemUser systemUser, Pageable pageable);
}
