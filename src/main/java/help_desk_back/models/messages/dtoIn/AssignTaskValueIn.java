package help_desk_back.models.messages.dtoIn;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssignTaskValueIn {
    Long executorId;
    Timestamp dueDate;
}
