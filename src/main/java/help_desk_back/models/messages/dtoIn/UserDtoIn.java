package help_desk_back.models.messages.dtoIn;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDtoIn {
    private Long id;
    private String name;
    private String family;
    private String eMail;
    private String password;
    private Long branchId;
    private List<String> privilegeId;
    private Long subdivisionId;
}
