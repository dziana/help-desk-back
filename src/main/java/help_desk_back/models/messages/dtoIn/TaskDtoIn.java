package help_desk_back.models.messages.dtoIn;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskDtoIn {
    private String name;
    private Long listenerId;
    private String text;
    private Long systemUserId;
    private Long executorId;
    private Long operatorId;
    private Long statusId;
}
