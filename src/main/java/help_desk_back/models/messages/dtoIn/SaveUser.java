package help_desk_back.models.messages.dtoIn;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SaveUser {
    private String name;
    private String family;
    private String email;
    private String password;
    private Long branchId;
    private Long subdivisionId;
}
