package help_desk_back.models.messages;

public enum RoleName {
    ROLE_USER,
    ROLE_EXECUTOR,
    ROLE_OPERATOR,
    ROLE_ADMIN
}