package help_desk_back.models.messages.dtoOut;

import help_desk_back.models.entities.SystemUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPageDtoOut {
    private List<SystemUser> content;
    private Long totalPages;
}
