package help_desk_back.models.messages.dtoOut;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShortUserDto {
    private Long id;
    private String name;
    private String family;
    private String eMail;
}
