package help_desk_back.models.messages.dtoOut;

import help_desk_back.models.entities.Listener;
import help_desk_back.models.entities.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskDtoOut {
    Long id;
    String name;
    Listener listenerId;
    String text;
    ShortUserDto systemUserId;
    ShortUserDto executorId;
    ShortUserDto operatorId;
    Status statusId;
    Date createdDate;
    Date executedDate;
    Date dueDate;
}