package help_desk_back.models.messages.dtoOut;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskPageDtoOut {
    private List<TaskDtoOut> content;
    private Long totalPages;
}
