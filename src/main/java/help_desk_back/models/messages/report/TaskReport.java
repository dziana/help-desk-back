package help_desk_back.models.messages.report;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskReport {
    private Long id;
    private String name;
    private Long listenerId;
    private String text;
    private Long systemUserId;
    private Long executorId;
    private Long operatorId;
    private String status;
    private Date createdDate;
    private Date executedDate;
    private Date dueDate;
}
