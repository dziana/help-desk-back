package help_desk_back.models.messages.report;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserReport {
    private Long id;
    private String name;
    private String family;
    private String eMail;
    private Long branchid;
    private String privilegeName;
    private Long subdivisionId;
}
