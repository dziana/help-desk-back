package help_desk_back.models.messages.report;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListenerReport {
    private Long id;
    private Long subdivisionId;
    private Long systemUserId;
}
