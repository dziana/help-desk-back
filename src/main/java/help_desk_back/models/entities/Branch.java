package help_desk_back.models.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "branches")
public class Branch extends BaseEntity{
    @Column(name = "name")
    private String name;
    @Column(name = "address")
    private String address;
}
