package help_desk_back.models.entities;

import help_desk_back.models.messages.RoleName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "privileges")
public class Privilege extends BaseEntity{
    @Column(name = "name")

    @Enumerated(EnumType.STRING)
    @NaturalId
    private RoleName name;

    public Privilege(Long id) {
        super(id);
    }
}
