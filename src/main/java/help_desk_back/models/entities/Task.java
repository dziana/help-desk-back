package help_desk_back.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tasks")
public class Task extends BaseEntity{
    @Column(name = "name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "listener_id")
    private Listener listenerId;
    @Column(name = "text")
    private String text;
    @ManyToOne
    @JoinColumn(name = "system_user_id")
    private SystemUser systemUserId;
    @ManyToOne
    @JoinColumn(name = "executor_id")
    private SystemUser executorId;
    @ManyToOne
    @JoinColumn(name = "operator_id")
    private SystemUser operatorId;
    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status statusId;
    @Column(name = "created_date")
    private Date createdDate;
    @Column(name = "executed_date")
    private Date executedDate;
    @Column(name = "due_date")
    private Date dueDate;
}
