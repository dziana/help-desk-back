package help_desk_back.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "system_users")
public class SystemUser extends BaseEntity{
    @Column(name = "name")
    private String name;
    @Column(name = "family")
    private String family;
    @Column(name = "e_mail")
    private String email;
    @Column(name = "password")
    @JsonIgnore
    private String password;
    @ManyToOne
    @JoinColumn(name = "branch_id")
    private Branch branchId;
    @JoinTable(name = "users_privileges",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "privilege_id"))
    @ManyToMany
    private Set<Privilege> privilege = new HashSet<>();
    @ManyToOne
    @JoinColumn(name = "subdivision_id")
    private Subdivision subdivisionId;
}
