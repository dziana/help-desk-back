package help_desk_back.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "subdivisions")
public class Subdivision extends BaseEntity{
    @Column(name = "name")
    private String name;
}
