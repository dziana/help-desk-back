package help_desk_back.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "listeners")
public class Listener extends BaseEntity {
    @OneToOne
    @JoinColumn(name = "user_id")
    private SystemUser systemUserId;
    @OneToOne
    @JoinColumn(name = "subdivision_id")
    private Subdivision subdivisionId;
}
