package help_desk_back.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
//@ComponentScan(basePackages = "help_desk_back.util")
@ComponentScan(basePackages = {"help_desk_back.services", "help_desk_back.controllers", "help_desk_back.util"})
@Import(RepositoryConfiguration.class)
public class TestConfiguration {

}
