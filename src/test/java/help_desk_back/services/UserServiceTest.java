//package help_desk_back.services;
//
//import help_desk_back.common.CommonTest;
//import help_desk_back.models.entities.Branch;
//import help_desk_back.models.entities.Privileges;
//import help_desk_back.models.entities.Subdivisions;
//import help_desk_back.models.entities.SystemUsers;
//import org.junit.Assert;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.util.*;
//
//public class UserServiceTest extends CommonTest {
//
//    @Autowired
//    private BranchesService branchesService;
//    @Autowired
//    private UserService userService;
//    @Autowired
//    private SubdivisionsService subdivisionsService;
//    @Autowired
//    private PrivilegeService privilegeService;
//
//    @Test
//    public void save() {
//        Branch branches = createAndSaveBranch();
//        Subdivisions subdivisions = createAndSaveSubdivision();
//        Privileges privileges = createAndSavePrivileges();
//        SystemUsers systemUsers = createAndSaveUser(branches, subdivisions, privileges);
//
//        Assert.assertEquals(systemUsers.getBranchId(), branches);
//        deleteUser(systemUsers, branches, subdivisions, privileges);
//    }
//
//    @Test
//    public void updatePrivilegeTestByUserId() {
//        Branch branches = createAndSaveBranch();
//        Subdivisions subdivisions = createAndSaveSubdivision();
//        Privileges privileges = createAndSavePrivileges();
//        SystemUsers systemUsers = createAndSaveUser(branches, subdivisions, privileges);
//
//        Privileges priv = new Privileges();
//        priv.setName("privilege");
//        Privileges privFromDb = privilegeService.save(priv);
//        List<Long> privList = new ArrayList<>();
//        Set<Privileges> setPriv = new HashSet<>();
//        setPriv.add(privFromDb);
//        privList.add(privFromDb.getId());
////        Set<Privileges> set = new HashSet<>();
////        set.add(priv);
////        systemUsers.setPrivilege(set);
////        userService.save(systemUsers);
//
//        userService.updateUserPrivileges(systemUsers.getId(), privList);
//
//        Optional<SystemUsers> optionalSystemUsers = userService.findById(systemUsers.getId());
//        SystemUsers systemUserFromDb = new SystemUsers();
//        if (optionalSystemUsers.isPresent()) {
//            systemUserFromDb = optionalSystemUsers.get();
//        }
//        System.out.println(systemUserFromDb.getPrivilege());
//
//        Assert.assertEquals(systemUserFromDb.getPrivilege(), setPriv);
//
//        privilegeService.delete(privFromDb);
//        deleteUser(systemUsers, branches, subdivisions, privileges);
//
//    }
//
//    private Branch createAndSaveBranch() {
//        Branch branches = new Branch();
//        branches.setName("californiya branch");
//        branches.setAddress("USA");
//        Branch save = branchesService.save(branches);
//        return save;
//    }
//
//    private Subdivisions createAndSaveSubdivision() {
//        Subdivisions subdivisions = new Subdivisions();
//        subdivisions.setName("managers");
//        Subdivisions subdivisionFromBd = subdivisionsService.save(subdivisions);
//        return subdivisionFromBd;
//    }
//
//    private Privileges createAndSavePrivileges() {
//        Privileges privileges = new Privileges();
//        privileges.setName("manager");
//        Privileges privilegeFromDb = privilegeService.save(privileges);
//        return privilegeFromDb;
//    }
//
//    private SystemUsers createAndSaveUser(Branch branches, Subdivisions subdivisions, Privileges privileges) {
//        SystemUsers systemUsers = new SystemUsers();
//        systemUsers.setName("diana");
//        systemUsers.setFamily("dogadina");
//        systemUsers.setEmail("ll@gmail");
//        systemUsers.setPassword("12345");
//        systemUsers.setSubdivisionId(subdivisions);
//        systemUsers.setBranchId(branches);
//        Set<Privileges> setPriv = new HashSet<Privileges>();
//        setPriv.add(privileges);
//        systemUsers.setPrivilege(setPriv);
//        SystemUsers userFromDb = userService.save(systemUsers);
//        return userFromDb;
//    }
//
//    private void deleteUser(SystemUsers systemUsers,
//                            Branch branches,
//                            Subdivisions subdivisions,
//                            Privileges privileges) {
//        userService.delete(systemUsers);
//        branchesService.delete(branches);
//        subdivisionsService.delete(subdivisions);
//        privilegeService.delete(privileges);
//    }
//}