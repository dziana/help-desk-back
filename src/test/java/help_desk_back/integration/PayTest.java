//package help_desk_back.integration;
//
//import com.lwo.bpay.core.utils.JsonMapper;
//import com.lwo.bpay.qr.SpringQrApplication;
//import com.lwo.bpay.qr.buisness.models.dto.AuthPost;
//import com.lwo.bpay.qr.buisness.models.dto.Purchase;
//import com.lwo.bpay.qr.buisness.models.dto.PurchasePost;
//import com.lwo.bpay.test.TestUtils;
//import com.lwo.bpay.test.jwt.TokenUtils;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.mock.web.MockHttpServletResponse;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.test.web.client.MockRestServiceServer;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.transaction.annotation.Transactional;
//
//import static org.junit.jupiter.api.Assertions.assertAll;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@SpringBootTest(classes = SpringQrApplication.class)
//@ExtendWith(SpringExtension.class)
//@AutoConfigureMockMvc
//@TestPropertySource(locations = "classpath:test.properties")
//@Transactional
//public class PayTest {
//    private final String jwtPath = "/jwt/jwt_14profile.json";
//
//    @Autowired
//    private MockMvc mvc;
//
//    @Autowired
//    JsonMapper jsonMapper;
//
//
//
//    @BeforeAll
//    public static void setUpPrivateKey() {
//        TokenUtils.setPrivateKeyPath("/jwt/privateKey.pem");
//    }
//
//    @Test
//    @DisplayName("Выполнение платежа")
//    public void payPostTest() throws Exception {
//        PurchasePost purchasePost = new PurchasePost().authCode(getAuthCode()).description("Test Payment").deviceId("1223").merchantId("MAGAZIN").orderNumber("5").sum(2.50);
//
//        MockHttpServletResponse response = mvc.perform(TestUtils.sendUnsecurePost("/pay", purchasePost))
//                .andExpect(status().is(200))
//                .andReturn().getResponse();
//        Purchase purchase = jsonMapper.toObject(response.getContentAsString(), Purchase.class);
//        assertAll(
//                () -> Assertions.assertNotNull(purchase.getAuthCode()),
//                () -> Assertions.assertNotNull(purchase.getCreatedDate())
//
//        );
//    }
//
//    private String getAuthCode() throws Exception {
//        AuthPost authPost = new AuthPost().purseId(262L);
//
//        return mvc.perform(TestUtils.sendPost("/auth", authPost,jwtPath))
//                .andExpect(status().is(200))
//                .andReturn().getResponse().getContentAsString();
//    }
//}
