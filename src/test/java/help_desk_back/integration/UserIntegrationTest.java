//package help_desk_back.integration;
//
//import help_desk_back.HelpDeskBackApplication;
//import help_desk_back.models.entities.SystemUsers;
//import help_desk_back.services.UserService;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.mock.web.MockHttpServletResponse;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.test.web.client.MockRestServiceServer;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.LinkedList;
//import java.util.List;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@SpringBootTest(classes = HelpDeskBackApplication.class)
//@ExtendWith(SpringExtension.class)
//@AutoConfigureMockMvc
//@Transactional
//public class UserIntegrationTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    UserService service;
//
//    @Test
//    @DisplayName("Get users")
//    public void userGetTest() throws Exception {
//        SystemUsers su= new SystemUsers();
//        su.setId(1L);
//        su.setName("diana");
//        su.setEmail("dd@mail.ru");
//        List<SystemUsers> list = new LinkedList<>();
//        list.add(su);
//        MockHttpServletResponse response = mockMvc.perform(
//                    get("/api/users").contentType(MediaType.APPLICATION_JSON))
//                    .andExpect(status().isOk())
//                    .andReturn().getResponse();
//        Assertions.assertNotNull(response.getContentAsString());
//    }
//}
