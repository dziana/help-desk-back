--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: branches; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.branches (
    id integer NOT NULL,
    name character varying(255),
    address character varying(255)
);


ALTER TABLE public.branches OWNER TO root;

--
-- Name: branch_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.branch_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.branch_id_seq OWNER TO root;

--
-- Name: branch_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.branch_id_seq OWNED BY public.branches.id;


--
-- Name: listeners; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.listeners (
    id integer NOT NULL,
    name character varying(100)
);


ALTER TABLE public.listeners OWNER TO root;

--
-- Name: listeners_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.listeners_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.listeners_id_seq OWNER TO root;

--
-- Name: listeners_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.listeners_id_seq OWNED BY public.listeners.id;


--
-- Name: privileges; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.privileges (
    id integer NOT NULL,
    name character varying(100) DEFAULT NULL::character varying
);


ALTER TABLE public.privileges OWNER TO root;

--
-- Name: privileges_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.privileges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.privileges_id_seq OWNER TO root;

--
-- Name: privileges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.privileges_id_seq OWNED BY public.privileges.id;


--
-- Name: problems; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.problems (
    id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.problems OWNER TO root;

--
-- Name: problems_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.problems_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.problems_id_seq OWNER TO root;

--
-- Name: problems_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.problems_id_seq OWNED BY public.problems.id;


--
-- Name: status; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.status (
    id integer NOT NULL,
    name character varying(20) NOT NULL
);


ALTER TABLE public.status OWNER TO root;

--
-- Name: status_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.status_id_seq OWNER TO root;

--
-- Name: status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.status_id_seq OWNED BY public.status.id;


--
-- Name: subdivisions; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.subdivisions (
    id integer NOT NULL,
    name character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.subdivisions OWNER TO root;

--
-- Name: subdivision_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.subdivision_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subdivision_id_seq OWNER TO root;

--
-- Name: subdivision_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.subdivision_id_seq OWNED BY public.subdivisions.id;


--
-- Name: system_users; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.system_users (
    id integer NOT NULL,
    name character varying(50) DEFAULT NULL::character varying,
    family character varying(50) DEFAULT NULL::character varying,
    e_mail character varying(50),
    password character varying(100) NOT NULL,
    branch_id integer,
    subdivision_id integer
);


ALTER TABLE public.system_users OWNER TO root;

--
-- Name: system_user_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.system_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_user_id_seq OWNER TO root;

--
-- Name: system_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.system_user_id_seq OWNED BY public.system_users.id;


--
-- Name: tasks; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.tasks (
    id integer NOT NULL,
    name character varying(50) DEFAULT NULL::character varying,
    listener_id integer,
    text text,
    system_user_id integer,
    executor_id integer,
    operator_id integer,
    status_id integer NOT NULL
);


ALTER TABLE public.tasks OWNER TO root;

--
-- Name: task_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.task_id_seq OWNER TO root;

--
-- Name: task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.task_id_seq OWNED BY public.tasks.id;


--
-- Name: users_privileges; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.users_privileges (
    user_id integer NOT NULL,
    privilege_id integer NOT NULL
);


ALTER TABLE public.users_privileges OWNER TO root;

--
-- Name: branches id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.branches ALTER COLUMN id SET DEFAULT nextval('public.branch_id_seq'::regclass);


--
-- Name: listeners id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.listeners ALTER COLUMN id SET DEFAULT nextval('public.listeners_id_seq'::regclass);


--
-- Name: privileges id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.privileges ALTER COLUMN id SET DEFAULT nextval('public.privileges_id_seq'::regclass);


--
-- Name: problems id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.problems ALTER COLUMN id SET DEFAULT nextval('public.problems_id_seq'::regclass);


--
-- Name: status id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.status ALTER COLUMN id SET DEFAULT nextval('public.status_id_seq'::regclass);


--
-- Name: subdivisions id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.subdivisions ALTER COLUMN id SET DEFAULT nextval('public.subdivision_id_seq'::regclass);


--
-- Name: system_users id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.system_users ALTER COLUMN id SET DEFAULT nextval('public.system_user_id_seq'::regclass);


--
-- Name: tasks id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tasks ALTER COLUMN id SET DEFAULT nextval('public.task_id_seq'::regclass);


--
-- Name: branch_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.branch_id_seq', 462, true);


--
-- Data for Name: branches; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.branches (id, name, address) FROM stdin;
410	Минск-северный	Минск
1	Колядичи	Минск
404	Степянка	Минск
3	Воронянского	Минск
440	Брест-литовская	Брест-литовская 13а
458	gomel branch	USA
441	californiya branch	USA
\.


--
-- Data for Name: listeners; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.listeners (id, name) FROM stdin;
2	Хозяйственный отдел
1	Программисты
92	Бухгалтерия
\.


--
-- Name: listeners_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.listeners_id_seq', 107, true);


--
-- Data for Name: privileges; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.privileges (id, name) FROM stdin;
2	ROLE_USER
3	ROLE_OPERATOR
1	ROLE_ADMIN
4	ROLE_EXECUTOR
\.


--
-- Name: privileges_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.privileges_id_seq', 71, true);


--
-- Data for Name: problems; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.problems (id, name) FROM stdin;
2	Принтеры
4	Картриджи
5	Интернет
6	Сапод
7	Товарная касса
8	Электронная перевозка
9	Счетфактуры
\.


--
-- Name: problems_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.problems_id_seq', 9, true);


--
-- Data for Name: status; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.status (id, name) FROM stdin;
1	SUBMITTED
2	PENDING
3	COMPLETED
4	CLOSED
\.


--
-- Name: status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.status_id_seq', 4, true);


--
-- Name: subdivision_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.subdivision_id_seq', 261, true);


--
-- Data for Name: subdivisions; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.subdivisions (id, name) FROM stdin;
1	Админы
2	Бухгалтерия\r\n
142	Маркетинг
229	Жэс №32
245	кролики
246	бабочки
\.


--
-- Name: system_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.system_user_id_seq', 678, true);


--
-- Data for Name: system_users; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.system_users (id, name, family, e_mail, password, branch_id, subdivision_id) FROM stdin;
23	Ярослав	Зыскунов	lkghost7@gmail.com	1	3	1
38	Станислав	Китовский	kit@nip.com	1	1	2
115	Татьяна	Малькова	tat@i.ru	1	3	1
22	Виталий	Ушаков	vinty@i.ua	1	1	2
174	Виталий Ушаков	Ушаков	vinty1978@gmail.com	11111	1	2
8	Di	123	123	123	3	1
643	Di	123	124	123	3	1
644	Di	123	125	123	3	1
646	Di	123	126	123	\N	\N
647	Di	123	127	123	\N	\N
648	Di	123	128	123	\N	\N
650	diana	dogadina	dd@gmail	12345	458	1
651	diana	dogadina	aa@gmail	12345	1	1
652	diana	dogadina	cc@gmail	12345	1	1
653	diana	dogadina	vv@gmail	12345	1	246
665	di	dogad	lll	12345	1	1
670	di	dogad	ll2	$2a$10$iuPH6/IvClw1B17DMhrSheDZQmHwH6FCdojlp5ivWaTbD9DZcDpwS	1	1
671	di	dogad	ll3	$2a$10$.tGVr1Wg1kgA.dHtV8CJoOTvHLTExCapZR/YG6ENsgnXmO4cHMTam	1	1
672	\N	dogad	ll4	$2a$10$kY8AQSM3mIdVUtJQKorHHucsvdukIn9lTU63l.5xFc45/Ww3YA7B2	1	1
673	di	dogad	ll5	$2a$10$mOaj2/1KGb4dseej53Sv..IGUHROY4W3yMK/pMTAB.L96pjR/W9sO	1	1
674	dian	dogad	ll6@mail.ru	$2a$10$LjCWNLrgfQqi27i2VZgkMuqFAzGSFZ21J0I3Xso26UmvxKclMGgPq	\N	\N
675	dian	dogad	129	$2a$10$HpBIYOwLvdkRV6SOxjVakua1Rw/OozU22L9tc0wGiGtZWRLpJ/mX.	\N	\N
676	dian	dogad	130	$2a$10$mekoYJQXkP3X/Tts2QYR2uPsWU41OOvrR2UqCZ0cHIg3RnrK.Q6f2	1	1
677	di	dog	dd@mail.ru	$2a$10$XGi5pUwbqC8ho24D5pbi.eOFyJODwhqd56ew/ps3nl079o7UHFNxm	\N	\N
678	di	dog	kk@mail	$2a$10$/NgXnw3n3sh91MhQmSK1oOxJP.qwYiV7PVMVy9tnKLFM.idnUoMeq	\N	\N
\.


--
-- Name: task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.task_id_seq', 303, true);


--
-- Data for Name: tasks; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.tasks (id, name, listener_id, text, system_user_id, executor_id, operator_id, status_id) FROM stdin;
68	Заявка №55	\N	Описание проблемы	\N	\N	\N	1
24	Levit	1	какой то сложный текст	23	22	23	1
25	Cenderel	2	нако использование	23	22	23	1
26	Gorton	1	аблица Orders	38	23	23	1
38	Заявка №1	1	текст заявки	22	22	23	1
39	Заявка №1	1	текст заявки	22	22	23	1
69	наименование заявки №33	\N	Проблема возникла недавно что то сделали с принтером	\N	\N	\N	1
303	new task	1	hay, it's my first task	676	22	23	1
89	Заявка №2	1	текст заявки	676	22	23	1
\.


--
-- Data for Name: users_privileges; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.users_privileges (user_id, privilege_id) FROM stdin;
22	1
23	3
23	4
174	2
652	3
653	3
665	4
670	2
671	2
673	2
674	2
675	2
676	2
677	2
678	2
678	1
\.


--
-- Name: branches branch_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.branches
    ADD CONSTRAINT branch_pkey PRIMARY KEY (id);


--
-- Name: listeners listeners_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.listeners
    ADD CONSTRAINT listeners_pkey PRIMARY KEY (id);


--
-- Name: privileges privileges_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.privileges
    ADD CONSTRAINT privileges_pkey PRIMARY KEY (id);


--
-- Name: problems problems_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.problems
    ADD CONSTRAINT problems_pkey PRIMARY KEY (id);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id);


--
-- Name: subdivisions subdivision_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.subdivisions
    ADD CONSTRAINT subdivision_pkey PRIMARY KEY (id);


--
-- Name: system_users system_user_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.system_users
    ADD CONSTRAINT system_user_pkey PRIMARY KEY (id);


--
-- Name: tasks task_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT task_pkey PRIMARY KEY (id);


--
-- Name: users_privileges users_privileges_user_id_privilege_id_pk; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_privileges
    ADD CONSTRAINT users_privileges_user_id_privilege_id_pk PRIMARY KEY (user_id, privilege_id);


--
-- Name: problems_name_uindex; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX problems_name_uindex ON public.problems USING btree (name);


--
-- Name: status_name_uindex; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX status_name_uindex ON public.status USING btree (name);


--
-- Name: system_users_e_mail_uindex; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX system_users_e_mail_uindex ON public.system_users USING btree (e_mail);


--
-- Name: system_users system_user_branch_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.system_users
    ADD CONSTRAINT system_user_branch_id_fk FOREIGN KEY (branch_id) REFERENCES public.branches(id);


--
-- Name: system_users system_user_subdivision_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.system_users
    ADD CONSTRAINT system_user_subdivision_id_fk FOREIGN KEY (subdivision_id) REFERENCES public.subdivisions(id);


--
-- Name: tasks task_executor_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT task_executor_id_fk FOREIGN KEY (executor_id) REFERENCES public.system_users(id);


--
-- Name: tasks task_operator_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT task_operator_id_fk FOREIGN KEY (operator_id) REFERENCES public.system_users(id);


--
-- Name: tasks task_system_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT task_system_user_id_fk FOREIGN KEY (system_user_id) REFERENCES public.system_users(id);


--
-- Name: tasks tasks_listeners_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks_listeners_id_fk FOREIGN KEY (listener_id) REFERENCES public.listeners(id);


--
-- Name: tasks tasks_status_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks_status_id_fk FOREIGN KEY (status_id) REFERENCES public.status(id);


--
-- Name: users_privileges users_privileges_privileges_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_privileges
    ADD CONSTRAINT users_privileges_privileges_id_fk FOREIGN KEY (privilege_id) REFERENCES public.privileges(id);


--
-- Name: users_privileges users_privileges_system_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users_privileges
    ADD CONSTRAINT users_privileges_system_user_id_fk FOREIGN KEY (user_id) REFERENCES public.system_users(id);


--
-- PostgreSQL database dump complete
--

